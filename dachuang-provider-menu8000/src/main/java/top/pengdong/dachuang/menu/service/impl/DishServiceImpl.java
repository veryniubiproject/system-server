package top.pengdong.dachuang.menu.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import top.pengdong.common.utils.PageUtils;
import top.pengdong.common.utils.Query;

import top.pengdong.dachuang.menu.dao.DishDao;
import top.pengdong.dachuang.menu.entity.DishEntity;
import top.pengdong.dachuang.menu.service.DishService;


@Service("dishService")
public class DishServiceImpl extends ServiceImpl<DishDao, DishEntity> implements DishService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DishEntity> page = this.page(
                new Query<DishEntity>().getPage(params),
                new QueryWrapper<DishEntity>()
        );

        return new PageUtils(page);
    }

}