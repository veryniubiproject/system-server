package top.pengdong.dachuang.menu.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import top.pengdong.common.utils.PageUtils;
import top.pengdong.common.utils.Query;

import top.pengdong.dachuang.menu.dao.StepDao;
import top.pengdong.dachuang.menu.entity.StepEntity;
import top.pengdong.dachuang.menu.service.StepService;


@Service("stepService")
public class StepServiceImpl extends ServiceImpl<StepDao, StepEntity> implements StepService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<StepEntity> page = this.page(
                new Query<StepEntity>().getPage(params),
                new QueryWrapper<StepEntity>()
        );

        return new PageUtils(page);
    }

}