package top.pengdong.dachuang.menu.dao;

import top.pengdong.dachuang.menu.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 分类表单，八种菜系或者其他的分类
 * 
 * @author chenshun
 * @email pengshi12138@outlook.com
 * @date 2022-03-14 21:16:34
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
