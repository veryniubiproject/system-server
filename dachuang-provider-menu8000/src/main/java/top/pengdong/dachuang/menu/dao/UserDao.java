package top.pengdong.dachuang.menu.dao;

import top.pengdong.dachuang.menu.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author chenshun
 * @email pengshi12138@outlook.com
 * @date 2022-03-14 13:45:10
 */
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {
	
}
