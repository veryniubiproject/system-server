package top.pengdong.dachuang.menu.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import top.pengdong.common.utils.PageUtils;
import top.pengdong.common.utils.R;
import top.pengdong.dachuang.menu.entity.StepEntity;
import top.pengdong.dachuang.menu.service.StepService;



/**
 * 
 *
 * @author chenshun
 * @email pengshi12138@outlook.com
 * @date 2022-03-14 13:45:10
 */
@RestController
@RequestMapping("menu/step")
public class StepController {
    @Autowired
    private StepService stepService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("menu:step:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = stepService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{stepId}")
    //@RequiresPermissions("menu:step:info")
    public R info(@PathVariable("stepId") Long stepId){
		StepEntity step = stepService.getById(stepId);

        return R.ok().put("step", step);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("menu:step:save")
    public R save(@RequestBody StepEntity step){
		stepService.save(step);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("menu:step:update")
    public R update(@RequestBody StepEntity step){
		stepService.updateById(step);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("menu:step:delete")
    public R delete(@RequestBody Long[] stepIds){
		stepService.removeByIds(Arrays.asList(stepIds));

        return R.ok();
    }

}
