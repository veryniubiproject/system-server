package top.pengdong.dachuang.menu.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author chenshun
 * @email pengshi12138@outlook.com
 * @date 2022-03-14 13:45:10
 */
@Data
@TableName("comment")
public class CommentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 评论ID
	 */
	@TableId
	private Long commentId;
	/**
	 * 评论时间
	 */
	private Date commentTime;
	/**
	 * 评论分数
	 */
	private BigDecimal commentScore;
	/**
	 * 评论内容
	 */
	private String commentDetail;

}
