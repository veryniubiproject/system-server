package top.pengdong.dachuang.menu.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author chenshun
 * @email pengshi12138@outlook.com
 * @date 2022-03-15 19:56:05
 */
@Data
@TableName("dish")
public class DishEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 菜品ID
	 */
	@TableId
	private Long dishId;
	/**
	 * 菜品名字
	 */
	private String dishName;
	/**
	 * 菜品价格
	 */
	private BigDecimal dishPrice;
	/**
	 * 菜品类型（八大菜系）
	 */
	private String dishCate;
	/**
	 * 菜品适宜人群
	 */
	private String suitableUser;
	/**
	 * 菜品标签（特色说明）
	 */
	private String dishFeature;
	/**
	 * 菜品材料
	 */
	private String dishMaterial;
	/**
	 * 工艺（炖、蒸、炸）
	 */
	private String menuCraft;
	/**
	 * 口味（酱香味、麻辣味等）
	 */
	private String menuTaste;
	/**
	 * 制作所需时间(单位是 min)
	 */
	private Integer menuTime;
	/**
	 * 主料
	 */
	private String mainIngredient;
	/**
	 * 辅料
	 */
	private String minorIngredient;
	/**
	 * 菜品图片展示
	 */
	private String imageUrl;
	/**
	 * 菜品图片展示2
	 */
	private String imageUrl2;

}
