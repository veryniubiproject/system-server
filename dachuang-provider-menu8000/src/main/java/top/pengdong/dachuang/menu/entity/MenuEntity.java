package top.pengdong.dachuang.menu.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author chenshun
 * @email pengshi12138@outlook.com
 * @date 2022-03-14 13:45:10
 */
@Data
@TableName("menu")
public class MenuEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 菜谱ID
	 */
	@TableId
	private Long menuId;
	/**
	 * 工艺（炖、蒸、炸）
	 */
	private String menuCraft;
	/**
	 * 口味（酱香味、麻辣味等）
	 */
	private String menuTaste;
	/**
	 * 制作所需时间
	 */
	private String menuTime;
	/**
	 * 主料
	 */
	private String mainIngredient;
	/**
	 * 辅料
	 */
	private String minorIngredient;

}
