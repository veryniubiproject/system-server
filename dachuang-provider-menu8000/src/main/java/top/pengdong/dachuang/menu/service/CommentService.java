package top.pengdong.dachuang.menu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.pengdong.common.utils.PageUtils;
import top.pengdong.dachuang.menu.entity.CommentEntity;

import java.util.Map;

/**
 * 
 *
 * @author chenshun
 * @email pengshi12138@outlook.com
 * @date 2022-03-14 13:45:10
 */
public interface CommentService extends IService<CommentEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

