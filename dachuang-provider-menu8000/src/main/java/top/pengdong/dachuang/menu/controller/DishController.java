package top.pengdong.dachuang.menu.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import top.pengdong.dachuang.menu.entity.DishEntity;
import top.pengdong.dachuang.menu.service.DishService;
import top.pengdong.common.utils.PageUtils;
import top.pengdong.common.utils.R;



/**
 * 
 *
 * @author chenshun
 * @email pengshi12138@outlook.com
 * @date 2022-03-15 19:56:05
 */
@RestController
@RequestMapping("menu/dish")
public class DishController {
    @Autowired
    private DishService dishService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = dishService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{dishId}")
    public R info(@PathVariable("dishId") Long dishId){
		DishEntity dish = dishService.getById(dishId);

        return R.ok().put("dish", dish);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody DishEntity dish){
		dishService.save(dish);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody DishEntity dish){
		dishService.updateById(dish);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] dishIds){
		dishService.removeByIds(Arrays.asList(dishIds));

        return R.ok();
    }

}
