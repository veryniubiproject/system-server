package top.pengdong.dachuang.menu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.pengdong.common.utils.PageUtils;
import top.pengdong.dachuang.menu.entity.DishEntity;

import java.util.Map;

/**
 * 
 *
 * @author chenshun
 * @email pengshi12138@outlook.com
 * @date 2022-03-15 19:56:05
 */
public interface DishService extends IService<DishEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

