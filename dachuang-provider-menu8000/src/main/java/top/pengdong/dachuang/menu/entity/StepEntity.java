package top.pengdong.dachuang.menu.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author chenshun
 * @email pengshi12138@outlook.com
 * @date 2022-03-14 13:45:10
 */
@Data
@TableName("step")
public class StepEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 步骤ID
	 */
	@TableId
	private Long stepId;
	/**
	 * 步骤次数
	 */
	private Integer stepSum;
	/**
	 * 步骤过程描述
	 */
	private String stepDesc;
	/**
	 * 步骤图片
	 */
	private String stepImg;

}
