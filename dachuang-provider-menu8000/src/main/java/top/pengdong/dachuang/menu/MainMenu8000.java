package top.pengdong.dachuang.menu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @description:
 * @projectName: dachuang
 * @see: top.pengdong.dachuang.menu
 * @author: pc
 * @createTime: 2022/3/14 13:38
 * @version: 1.0
 */
@EnableDiscoveryClient
@SpringBootApplication
public class MainMenu8000 {
	public static void main(String[] args) {
		SpringApplication.run(MainMenu8000.class, args);
	}
}
