package top.pengdong.dachuang.menu.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author chenshun
 * @email pengshi12138@outlook.com
 * @date 2022-03-14 13:45:10
 */
@Data
@TableName("user")
public class UserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户ID
	 */
	@TableId
	private Long userId;
	/**
	 * 用户姓名
	 */
	private String username;
	/**
	 * 用户密码
	 */
	private String password;
	/**
	 * 用户创建时间
	 */
	private Date creationTime;
	/**
	 * 用户忌讳
	 */
	private String userHate;
	/**
	 * 用户昵称
	 */
	private String nickname;
	/**
	 * 用户电话
	 */
	private String phone;
	/**
	 * 用户头像
	 */
	private String imageurl;
	/**
	 * 用户微信id
	 */
	private String openid;
	/**
	 * 用户描述 自我介绍
	 */
	private String description;

}
