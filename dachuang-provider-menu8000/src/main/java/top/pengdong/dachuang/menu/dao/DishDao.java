package top.pengdong.dachuang.menu.dao;

import top.pengdong.dachuang.menu.entity.DishEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author chenshun
 * @email pengshi12138@outlook.com
 * @date 2022-03-15 19:56:05
 */
@Mapper
public interface DishDao extends BaseMapper<DishEntity> {
	
}
