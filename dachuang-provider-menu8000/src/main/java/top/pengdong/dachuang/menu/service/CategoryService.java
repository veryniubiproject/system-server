package top.pengdong.dachuang.menu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.pengdong.common.utils.PageUtils;
import top.pengdong.dachuang.menu.entity.CategoryEntity;

import java.util.List;
import java.util.Map;

/**
 * 分类表单，八种菜系或者其他的分类
 *
 * @author chenshun
 * @email pengshi12138@outlook.com
 * @date 2022-03-14 21:16:34
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CategoryEntity> listWithTree();

	void removeMenuByIds(List<Long> asList);
}

