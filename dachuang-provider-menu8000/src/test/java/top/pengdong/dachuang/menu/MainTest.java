package top.pengdong.dachuang.menu;

import com.aliyun.oss.OSSClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import top.pengdong.dachuang.menu.dao.DishDao;
import top.pengdong.dachuang.menu.entity.CommentEntity;
import top.pengdong.dachuang.menu.entity.DishEntity;
import top.pengdong.dachuang.menu.service.CommentService;
import top.pengdong.dachuang.menu.service.MenuService;

import javax.annotation.Resource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.math.BigDecimal;

/**
 * @description:
 * @projectName: dachuang
 * @see: top.pengdong.dachuang.menu
 * @author: pc
 * @createTime: 2022/3/14 14:32
 * @version: 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MainTest {

	@Resource
	private CommentService commentService;

	@Test
	public void contextLoads() {
		CommentEntity comment = new CommentEntity();
		comment.setCommentDetail("真不错啊");
		comment.setCommentScore(BigDecimal.valueOf(5.0));
		commentService.save(comment);
		System.out.println("保存成功");
	}

	@Test
	public void contextLoadsUpdate() {
		CommentEntity comment = new CommentEntity();
		comment.setCommentDetail("真不错啊");
		comment.setCommentScore(BigDecimal.valueOf(5.0));
		commentService.save(comment);
		System.out.println("保存成功");
	}

	@Resource
	private DishDao dao;




	@Resource
	private OSSClient ossClient;
	@Test
	public void testPic() throws FileNotFoundException {
		String filePath= "C:\\Users\\22806\\Downloads\\diugai.com (1).png";
		InputStream inputStream = new FileInputStream(filePath);
		ossClient.putObject("dachuang-pictrue", "good.jpg", inputStream);
	}

}


