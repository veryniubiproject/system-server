package top.pengdong.dachuang.thirdPart;

import com.aliyun.oss.OSSClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * @description:
 * @projectName: dachuang
 * @see: top.pengdong.dachuang.thirdPart
 * @author: pc
 * @createTime: 2022/3/16 11:14
 * @version: 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestMain {
	@Resource
	private OSSClient ossClient;
	@Test
	public void testPic() throws FileNotFoundException {
		String filePath= "C:\\Users\\22806\\Downloads\\diugai.com (1).png";
		InputStream inputStream = new FileInputStream(filePath);
		ossClient.putObject("dachuang-pictrue", "good.jpg", inputStream);
	}
}
