package top.pengdong.dachuang.thirdPart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @description:
 * @projectName: dachuang
 * @see: top.pengdong.dachuang
 * @author: pc
 * @createTime: 2022/3/16 11:08
 * @version: 1.0
 */
@EnableDiscoveryClient
@SpringBootApplication
public class MainThirdPart {
	public static void main(String[] args) {
		SpringApplication.run(MainThirdPart.class, args);
	}
}
