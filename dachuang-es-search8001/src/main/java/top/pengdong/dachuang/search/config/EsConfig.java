package top.pengdong.dachuang.search.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description: 配置 请求 远程es
 * @projectName: dachuang
 * @see: top.pengdong.dachuang.main.config
 * @author: pc
 * @createTime: 2022/3/12 22:30
 * @version: 1.0
 */
@Configuration
public class EsConfig {

	@Bean
	public RestHighLevelClient esRestClient() {
		RestHighLevelClient client = new RestHighLevelClient(
				RestClient.builder(
						new HttpHost("112.74.72.119", 9200, "http")));
		return client;
	}
}
