package top.pengdong.dachuang.search;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description:
 * @projectName: dachuang
 * @see: top.pengdong.dachuang
 * @author: pc
 * @createTime: 2022/3/12 22:29
 * @version: 1.0
 */
@SpringBootApplication
public class EsSearchMain8001 {
	public static void main(String[] args) {
		SpringApplication.run(EsSearchMain8001.class, args);
	}
}
