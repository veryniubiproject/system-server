package top.pengdong.dachuang.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsConfigurationSource;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

/**
 * @description: 解决跨域请求问题
 * @projectName: dachuang
 * @see: top.pengdong.dachuang.gateway.config
 * @author: pc
 * @createTime: 2022/3/15 10:33
 * @version: 1.0
 */
@Configuration
public class CorsConfig {
	@Bean
	public CorsWebFilter corsWebFilter() {
		UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
		CorsConfiguration corsConfigurationSource = new CorsConfiguration();
		// 配置跨域
		corsConfigurationSource.addAllowedHeader("*");
		corsConfigurationSource.addAllowedMethod("*");
		corsConfigurationSource.addAllowedOrigin("*");
		corsConfigurationSource.setAllowCredentials(true);


		urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfigurationSource);
		return new CorsWebFilter(urlBasedCorsConfigurationSource);
	}
}
