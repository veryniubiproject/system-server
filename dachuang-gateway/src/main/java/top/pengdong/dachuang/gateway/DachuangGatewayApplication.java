package top.pengdong.dachuang.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
public class DachuangGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(DachuangGatewayApplication.class, args);
	}

}
