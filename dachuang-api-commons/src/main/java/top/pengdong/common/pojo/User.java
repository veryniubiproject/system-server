package top.pengdong.common.pojo;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
    /**
     * 用户ID
     *
     * @mbggenerated
     */
    private Long userId;

    /**
     * 用户姓名
     *
     * @mbggenerated
     */
    private String username;

    /**
     * 用户密码
     *
     * @mbggenerated
     */
    private String password;

    /**
     * 用户创建时间
     *
     * @mbggenerated
     */
    private Date creationTime;

    /**
     * 用户忌讳
     *
     * @mbggenerated
     */
    private String userHate;

    /**
     * 用户昵称
     *
     * @mbggenerated
     */
    private String nickname;

    /**
     * 用户电话
     *
     * @mbggenerated
     */
    private String phone;

    /**
     * 用户头像
     *
     * @mbggenerated
     */
    private String imageurl;

    /**
     * 用户微信id
     *
     * @mbggenerated
     */
    private String openid;

    /**
     * 用户描述 自我介绍
     *
     * @mbggenerated
     */
    private String description;

    private static final long serialVersionUID = 1L;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getUserHate() {
        return userHate;
    }

    public void setUserHate(String userHate) {
        this.userHate = userHate;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", userId=").append(userId);
        sb.append(", username=").append(username);
        sb.append(", password=").append(password);
        sb.append(", creationTime=").append(creationTime);
        sb.append(", userHate=").append(userHate);
        sb.append(", nickname=").append(nickname);
        sb.append(", phone=").append(phone);
        sb.append(", imageurl=").append(imageurl);
        sb.append(", openid=").append(openid);
        sb.append(", description=").append(description);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}