package top.pengdong.common.utils;

import lombok.Getter;
import lombok.Setter;


/**
 * @desc 自定义响应码
 * @author pengshi
 * @date 2022/1/18 19:33
 **/
public enum ResultCode {
	OK(200,"访问成功"),
	FAIL(500,"服务器异常"),
	UNKNOWN_ERROR(503,"未知异常"),
	TOKEN_INVALID(406,"TOKEN失效"),
	SIGN_ERROR(403,"权限不足，请联系管理人员"),
	USER_NOT_EXIST(405,"尚未登陆，请登录"),
	USER_PASS_ERROR(401,"用户名或密码错误"),
	NOT_EXIST_ERROR(404, "资源不存在"),
	CODE_PASS_ERROR(402, "验证码错误"),
	REGISTER_TIME_ERROR(400, "激活失败！激活码过期"),
	REGISTER_ERROR(400, "激活失败！用户名存在");

	@Setter
	@Getter
	public final int code;
	@Setter
	@Getter
	public final String msg;
	
	ResultCode(int code, String msg){
		this.code = code;
		this.msg = msg;
	}
}