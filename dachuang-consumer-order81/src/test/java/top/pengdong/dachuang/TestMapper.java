package top.pengdong.dachuang;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import top.pengdong.dachuang.main.MainApplication81;
import top.pengdong.dachuang.main.dao.UserMapper;
import top.pengdong.dachuang.main.mbg.model.UserExample;
import top.pengdong.common.pojo.User;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description:
 * @projectName: dachuang
 * @see: PACKAGE_NAME
 * @author: pc
 * @createTime: 2022/3/11 14:06
 * @version: 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MainApplication81.class)
public class TestMapper {

	@Resource
	private PasswordEncoder passwordEncoder;

	@Resource
	private UserMapper userMapper;

	@Test
	public void testMapperUser() {
		UserExample example = new UserExample();
		UserExample.Criteria criteria = example.createCriteria();
		criteria.andUsernameEqualTo("pengshi");
		List<User> list = userMapper.selectByExample(example);
		System.out.println(list.get(0));
	}
}
