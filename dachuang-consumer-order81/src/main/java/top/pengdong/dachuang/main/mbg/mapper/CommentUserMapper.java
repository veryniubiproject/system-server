package top.pengdong.dachuang.main.mbg.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import top.pengdong.dachuang.main.mbg.model.CommentUser;
import top.pengdong.dachuang.main.mbg.model.CommentUserExample;

public interface CommentUserMapper {
    int countByExample(CommentUserExample example);

    int deleteByExample(CommentUserExample example);

    int deleteByPrimaryKey(Long id);

    int insert(CommentUser record);

    int insertSelective(CommentUser record);

    List<CommentUser> selectByExample(CommentUserExample example);

    CommentUser selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") CommentUser record, @Param("example") CommentUserExample example);

    int updateByExample(@Param("record") CommentUser record, @Param("example") CommentUserExample example);

    int updateByPrimaryKeySelective(CommentUser record);

    int updateByPrimaryKey(CommentUser record);
}