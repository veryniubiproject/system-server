package top.pengdong.dachuang.main.controller;

import org.springframework.web.bind.annotation.*;
import top.pengdong.common.utils.ResultResponse;
import top.pengdong.dachuang.main.service.UserService;

import javax.annotation.Resource;

/**
 * @description: 登陆接口设计
 * @projectName: dachuang
 * @see: top.pengdong.dachuang.main.controller
 * @author: pc
 * @createTime: 2022/3/11 11:15
 * @version: 1.0
 */
@RestController
public class LoginController {

	@Resource
	private UserService userService;

	@PostMapping("/login")
	public ResultResponse login(@RequestParam String username, @RequestParam String password, @RequestParam(required = false) String code) {
		return userService.login(username, password, code);
	}

	@GetMapping("/logout")
	public ResultResponse logout() {
		return ResultResponse.ok("登出成功");
	}

	@GetMapping("/userInfo")
	public ResultResponse userInfo(@RequestHeader("Authorization") String headerValue) {
		return userService.getUserInfo(headerValue);
	}
}
