package top.pengdong.dachuang.main.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.pengdong.common.utils.R;
import top.pengdong.dachuang.main.fegin.CommentService;

import javax.annotation.Resource;

/**
 * @description:
 * @projectName: dachuang
 * @see: top.pengdong.dachuang.main.controller
 * @author: pc
 * @createTime: 2022/3/11 0:30
 * @version: 1.0
 */
@RestController
@RequestMapping("/test")
public class TestController {
	@Resource
	private CommentService commentService;

	@RequestMapping("/commonList")
	public R test1() {
		return R.ok().put("list",commentService.getCommentList());
	}

	@RequestMapping("/hello")
	public String hello() {
		return "hello";
	}

	@RequestMapping("/test")
	public String test() {
		return "test";
	}
}
