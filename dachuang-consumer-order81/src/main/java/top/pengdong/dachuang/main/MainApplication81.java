package top.pengdong.dachuang.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @description:
 * @projectName: dachuang
 * @see: top.pengdong.dachuang
 * @author: pc
 * @createTime: 2022/3/10 23:38
 * @version: 1.0
 */
@EnableFeignClients(basePackages = "top.pengdong.dachuang.main.fegin")
@EnableDiscoveryClient
@SpringBootApplication
public class MainApplication81 {
	public static void main(String[] args) {
		SpringApplication.run(MainApplication81.class, args);
	}
}
