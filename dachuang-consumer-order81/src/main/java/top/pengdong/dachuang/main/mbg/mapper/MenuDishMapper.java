package top.pengdong.dachuang.main.mbg.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import top.pengdong.dachuang.main.mbg.model.MenuDish;
import top.pengdong.dachuang.main.mbg.model.MenuDishExample;

public interface MenuDishMapper {
    int countByExample(MenuDishExample example);

    int deleteByExample(MenuDishExample example);

    int deleteByPrimaryKey(Long id);

    int insert(MenuDish record);

    int insertSelective(MenuDish record);

    List<MenuDish> selectByExample(MenuDishExample example);

    MenuDish selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") MenuDish record, @Param("example") MenuDishExample example);

    int updateByExample(@Param("record") MenuDish record, @Param("example") MenuDishExample example);

    int updateByPrimaryKeySelective(MenuDish record);

    int updateByPrimaryKey(MenuDish record);
}