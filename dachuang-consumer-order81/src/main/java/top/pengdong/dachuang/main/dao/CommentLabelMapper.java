package top.pengdong.dachuang.main.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import top.pengdong.dachuang.main.mbg.model.CommentLabel;
import top.pengdong.dachuang.main.mbg.model.CommentLabelExample;

public interface CommentLabelMapper {
    int countByExample(CommentLabelExample example);

    int deleteByExample(CommentLabelExample example);

    int deleteByPrimaryKey(Long commentLabelId);

    int insert(CommentLabel record);

    int insertSelective(CommentLabel record);

    List<CommentLabel> selectByExample(CommentLabelExample example);

    CommentLabel selectByPrimaryKey(Long commentLabelId);

    int updateByExampleSelective(@Param("record") CommentLabel record, @Param("example") CommentLabelExample example);

    int updateByExample(@Param("record") CommentLabel record, @Param("example") CommentLabelExample example);

    int updateByPrimaryKeySelective(CommentLabel record);

    int updateByPrimaryKey(CommentLabel record);
}