package top.pengdong.dachuang.main.mbg.model;

import java.io.Serializable;

public class Menu implements Serializable {
    /**
     * 菜谱ID
     *
     * @mbggenerated
     */
    private Long menuId;

    /**
     * 工艺（炖、蒸、炸）
     *
     * @mbggenerated
     */
    private String menuCraft;

    /**
     * 口味（酱香味、麻辣味等）
     *
     * @mbggenerated
     */
    private String menuTaste;

    /**
     * 制作所需时间
     *
     * @mbggenerated
     */
    private String menuTime;

    /**
     * 主料
     *
     * @mbggenerated
     */
    private String mainIngredient;

    /**
     * 辅料
     *
     * @mbggenerated
     */
    private String minorIngredient;

    private static final long serialVersionUID = 1L;

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public String getMenuCraft() {
        return menuCraft;
    }

    public void setMenuCraft(String menuCraft) {
        this.menuCraft = menuCraft;
    }

    public String getMenuTaste() {
        return menuTaste;
    }

    public void setMenuTaste(String menuTaste) {
        this.menuTaste = menuTaste;
    }

    public String getMenuTime() {
        return menuTime;
    }

    public void setMenuTime(String menuTime) {
        this.menuTime = menuTime;
    }

    public String getMainIngredient() {
        return mainIngredient;
    }

    public void setMainIngredient(String mainIngredient) {
        this.mainIngredient = mainIngredient;
    }

    public String getMinorIngredient() {
        return minorIngredient;
    }

    public void setMinorIngredient(String minorIngredient) {
        this.minorIngredient = minorIngredient;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", menuId=").append(menuId);
        sb.append(", menuCraft=").append(menuCraft);
        sb.append(", menuTaste=").append(menuTaste);
        sb.append(", menuTime=").append(menuTime);
        sb.append(", mainIngredient=").append(mainIngredient);
        sb.append(", minorIngredient=").append(minorIngredient);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}