package top.pengdong.dachuang.main.mbg.model;

import java.io.Serializable;

public class CommentLabel implements Serializable {
    /**
     * 评论标签ID
     *
     * @mbggenerated
     */
    private Long commentLabelId;

    /**
     * 菜品ID
     *
     * @mbggenerated
     */
    private String dishId;

    /**
     * 标签名字
     *
     * @mbggenerated
     */
    private String labelName;

    /**
     * 标签出现次数
     *
     * @mbggenerated
     */
    private Integer frequency;

    private static final long serialVersionUID = 1L;

    public Long getCommentLabelId() {
        return commentLabelId;
    }

    public void setCommentLabelId(Long commentLabelId) {
        this.commentLabelId = commentLabelId;
    }

    public String getDishId() {
        return dishId;
    }

    public void setDishId(String dishId) {
        this.dishId = dishId;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", commentLabelId=").append(commentLabelId);
        sb.append(", dishId=").append(dishId);
        sb.append(", labelName=").append(labelName);
        sb.append(", frequency=").append(frequency);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}