package top.pengdong.dachuang.main.mbg.model;

import java.util.ArrayList;
import java.util.List;

public class StepExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public StepExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andStepIdIsNull() {
            addCriterion("step_id is null");
            return (Criteria) this;
        }

        public Criteria andStepIdIsNotNull() {
            addCriterion("step_id is not null");
            return (Criteria) this;
        }

        public Criteria andStepIdEqualTo(Long value) {
            addCriterion("step_id =", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdNotEqualTo(Long value) {
            addCriterion("step_id <>", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdGreaterThan(Long value) {
            addCriterion("step_id >", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdGreaterThanOrEqualTo(Long value) {
            addCriterion("step_id >=", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdLessThan(Long value) {
            addCriterion("step_id <", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdLessThanOrEqualTo(Long value) {
            addCriterion("step_id <=", value, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdIn(List<Long> values) {
            addCriterion("step_id in", values, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdNotIn(List<Long> values) {
            addCriterion("step_id not in", values, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdBetween(Long value1, Long value2) {
            addCriterion("step_id between", value1, value2, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepIdNotBetween(Long value1, Long value2) {
            addCriterion("step_id not between", value1, value2, "stepId");
            return (Criteria) this;
        }

        public Criteria andStepSumIsNull() {
            addCriterion("step_sum is null");
            return (Criteria) this;
        }

        public Criteria andStepSumIsNotNull() {
            addCriterion("step_sum is not null");
            return (Criteria) this;
        }

        public Criteria andStepSumEqualTo(Integer value) {
            addCriterion("step_sum =", value, "stepSum");
            return (Criteria) this;
        }

        public Criteria andStepSumNotEqualTo(Integer value) {
            addCriterion("step_sum <>", value, "stepSum");
            return (Criteria) this;
        }

        public Criteria andStepSumGreaterThan(Integer value) {
            addCriterion("step_sum >", value, "stepSum");
            return (Criteria) this;
        }

        public Criteria andStepSumGreaterThanOrEqualTo(Integer value) {
            addCriterion("step_sum >=", value, "stepSum");
            return (Criteria) this;
        }

        public Criteria andStepSumLessThan(Integer value) {
            addCriterion("step_sum <", value, "stepSum");
            return (Criteria) this;
        }

        public Criteria andStepSumLessThanOrEqualTo(Integer value) {
            addCriterion("step_sum <=", value, "stepSum");
            return (Criteria) this;
        }

        public Criteria andStepSumIn(List<Integer> values) {
            addCriterion("step_sum in", values, "stepSum");
            return (Criteria) this;
        }

        public Criteria andStepSumNotIn(List<Integer> values) {
            addCriterion("step_sum not in", values, "stepSum");
            return (Criteria) this;
        }

        public Criteria andStepSumBetween(Integer value1, Integer value2) {
            addCriterion("step_sum between", value1, value2, "stepSum");
            return (Criteria) this;
        }

        public Criteria andStepSumNotBetween(Integer value1, Integer value2) {
            addCriterion("step_sum not between", value1, value2, "stepSum");
            return (Criteria) this;
        }

        public Criteria andStepDescIsNull() {
            addCriterion("step_desc is null");
            return (Criteria) this;
        }

        public Criteria andStepDescIsNotNull() {
            addCriterion("step_desc is not null");
            return (Criteria) this;
        }

        public Criteria andStepDescEqualTo(String value) {
            addCriterion("step_desc =", value, "stepDesc");
            return (Criteria) this;
        }

        public Criteria andStepDescNotEqualTo(String value) {
            addCriterion("step_desc <>", value, "stepDesc");
            return (Criteria) this;
        }

        public Criteria andStepDescGreaterThan(String value) {
            addCriterion("step_desc >", value, "stepDesc");
            return (Criteria) this;
        }

        public Criteria andStepDescGreaterThanOrEqualTo(String value) {
            addCriterion("step_desc >=", value, "stepDesc");
            return (Criteria) this;
        }

        public Criteria andStepDescLessThan(String value) {
            addCriterion("step_desc <", value, "stepDesc");
            return (Criteria) this;
        }

        public Criteria andStepDescLessThanOrEqualTo(String value) {
            addCriterion("step_desc <=", value, "stepDesc");
            return (Criteria) this;
        }

        public Criteria andStepDescLike(String value) {
            addCriterion("step_desc like", value, "stepDesc");
            return (Criteria) this;
        }

        public Criteria andStepDescNotLike(String value) {
            addCriterion("step_desc not like", value, "stepDesc");
            return (Criteria) this;
        }

        public Criteria andStepDescIn(List<String> values) {
            addCriterion("step_desc in", values, "stepDesc");
            return (Criteria) this;
        }

        public Criteria andStepDescNotIn(List<String> values) {
            addCriterion("step_desc not in", values, "stepDesc");
            return (Criteria) this;
        }

        public Criteria andStepDescBetween(String value1, String value2) {
            addCriterion("step_desc between", value1, value2, "stepDesc");
            return (Criteria) this;
        }

        public Criteria andStepDescNotBetween(String value1, String value2) {
            addCriterion("step_desc not between", value1, value2, "stepDesc");
            return (Criteria) this;
        }

        public Criteria andStepImgIsNull() {
            addCriterion("step_img is null");
            return (Criteria) this;
        }

        public Criteria andStepImgIsNotNull() {
            addCriterion("step_img is not null");
            return (Criteria) this;
        }

        public Criteria andStepImgEqualTo(String value) {
            addCriterion("step_img =", value, "stepImg");
            return (Criteria) this;
        }

        public Criteria andStepImgNotEqualTo(String value) {
            addCriterion("step_img <>", value, "stepImg");
            return (Criteria) this;
        }

        public Criteria andStepImgGreaterThan(String value) {
            addCriterion("step_img >", value, "stepImg");
            return (Criteria) this;
        }

        public Criteria andStepImgGreaterThanOrEqualTo(String value) {
            addCriterion("step_img >=", value, "stepImg");
            return (Criteria) this;
        }

        public Criteria andStepImgLessThan(String value) {
            addCriterion("step_img <", value, "stepImg");
            return (Criteria) this;
        }

        public Criteria andStepImgLessThanOrEqualTo(String value) {
            addCriterion("step_img <=", value, "stepImg");
            return (Criteria) this;
        }

        public Criteria andStepImgLike(String value) {
            addCriterion("step_img like", value, "stepImg");
            return (Criteria) this;
        }

        public Criteria andStepImgNotLike(String value) {
            addCriterion("step_img not like", value, "stepImg");
            return (Criteria) this;
        }

        public Criteria andStepImgIn(List<String> values) {
            addCriterion("step_img in", values, "stepImg");
            return (Criteria) this;
        }

        public Criteria andStepImgNotIn(List<String> values) {
            addCriterion("step_img not in", values, "stepImg");
            return (Criteria) this;
        }

        public Criteria andStepImgBetween(String value1, String value2) {
            addCriterion("step_img between", value1, value2, "stepImg");
            return (Criteria) this;
        }

        public Criteria andStepImgNotBetween(String value1, String value2) {
            addCriterion("step_img not between", value1, value2, "stepImg");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}