package top.pengdong.dachuang.main.mbg.model;

import java.io.Serializable;

public class Step implements Serializable {
    /**
     * 步骤ID
     *
     * @mbggenerated
     */
    private Long stepId;

    /**
     * 步骤次数
     *
     * @mbggenerated
     */
    private Integer stepSum;

    /**
     * 步骤过程描述
     *
     * @mbggenerated
     */
    private String stepDesc;

    /**
     * 步骤图片
     *
     * @mbggenerated
     */
    private String stepImg;

    private static final long serialVersionUID = 1L;

    public Long getStepId() {
        return stepId;
    }

    public void setStepId(Long stepId) {
        this.stepId = stepId;
    }

    public Integer getStepSum() {
        return stepSum;
    }

    public void setStepSum(Integer stepSum) {
        this.stepSum = stepSum;
    }

    public String getStepDesc() {
        return stepDesc;
    }

    public void setStepDesc(String stepDesc) {
        this.stepDesc = stepDesc;
    }

    public String getStepImg() {
        return stepImg;
    }

    public void setStepImg(String stepImg) {
        this.stepImg = stepImg;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", stepId=").append(stepId);
        sb.append(", stepSum=").append(stepSum);
        sb.append(", stepDesc=").append(stepDesc);
        sb.append(", stepImg=").append(stepImg);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}