package top.pengdong.dachuang.main.mbg.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import top.pengdong.dachuang.main.mbg.model.DishComment;
import top.pengdong.dachuang.main.mbg.model.DishCommentExample;

public interface DishCommentMapper {
    int countByExample(DishCommentExample example);

    int deleteByExample(DishCommentExample example);

    int deleteByPrimaryKey(Long id);

    int insert(DishComment record);

    int insertSelective(DishComment record);

    List<DishComment> selectByExample(DishCommentExample example);

    DishComment selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") DishComment record, @Param("example") DishCommentExample example);

    int updateByExample(@Param("record") DishComment record, @Param("example") DishCommentExample example);

    int updateByPrimaryKeySelective(DishComment record);

    int updateByPrimaryKey(DishComment record);
}