package top.pengdong.dachuang.main.mbg.model;

import java.util.ArrayList;
import java.util.List;

public class MenuExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public MenuExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMenuIdIsNull() {
            addCriterion("menu_id is null");
            return (Criteria) this;
        }

        public Criteria andMenuIdIsNotNull() {
            addCriterion("menu_id is not null");
            return (Criteria) this;
        }

        public Criteria andMenuIdEqualTo(Long value) {
            addCriterion("menu_id =", value, "menuId");
            return (Criteria) this;
        }

        public Criteria andMenuIdNotEqualTo(Long value) {
            addCriterion("menu_id <>", value, "menuId");
            return (Criteria) this;
        }

        public Criteria andMenuIdGreaterThan(Long value) {
            addCriterion("menu_id >", value, "menuId");
            return (Criteria) this;
        }

        public Criteria andMenuIdGreaterThanOrEqualTo(Long value) {
            addCriterion("menu_id >=", value, "menuId");
            return (Criteria) this;
        }

        public Criteria andMenuIdLessThan(Long value) {
            addCriterion("menu_id <", value, "menuId");
            return (Criteria) this;
        }

        public Criteria andMenuIdLessThanOrEqualTo(Long value) {
            addCriterion("menu_id <=", value, "menuId");
            return (Criteria) this;
        }

        public Criteria andMenuIdIn(List<Long> values) {
            addCriterion("menu_id in", values, "menuId");
            return (Criteria) this;
        }

        public Criteria andMenuIdNotIn(List<Long> values) {
            addCriterion("menu_id not in", values, "menuId");
            return (Criteria) this;
        }

        public Criteria andMenuIdBetween(Long value1, Long value2) {
            addCriterion("menu_id between", value1, value2, "menuId");
            return (Criteria) this;
        }

        public Criteria andMenuIdNotBetween(Long value1, Long value2) {
            addCriterion("menu_id not between", value1, value2, "menuId");
            return (Criteria) this;
        }

        public Criteria andMenuCraftIsNull() {
            addCriterion("menu_craft is null");
            return (Criteria) this;
        }

        public Criteria andMenuCraftIsNotNull() {
            addCriterion("menu_craft is not null");
            return (Criteria) this;
        }

        public Criteria andMenuCraftEqualTo(String value) {
            addCriterion("menu_craft =", value, "menuCraft");
            return (Criteria) this;
        }

        public Criteria andMenuCraftNotEqualTo(String value) {
            addCriterion("menu_craft <>", value, "menuCraft");
            return (Criteria) this;
        }

        public Criteria andMenuCraftGreaterThan(String value) {
            addCriterion("menu_craft >", value, "menuCraft");
            return (Criteria) this;
        }

        public Criteria andMenuCraftGreaterThanOrEqualTo(String value) {
            addCriterion("menu_craft >=", value, "menuCraft");
            return (Criteria) this;
        }

        public Criteria andMenuCraftLessThan(String value) {
            addCriterion("menu_craft <", value, "menuCraft");
            return (Criteria) this;
        }

        public Criteria andMenuCraftLessThanOrEqualTo(String value) {
            addCriterion("menu_craft <=", value, "menuCraft");
            return (Criteria) this;
        }

        public Criteria andMenuCraftLike(String value) {
            addCriterion("menu_craft like", value, "menuCraft");
            return (Criteria) this;
        }

        public Criteria andMenuCraftNotLike(String value) {
            addCriterion("menu_craft not like", value, "menuCraft");
            return (Criteria) this;
        }

        public Criteria andMenuCraftIn(List<String> values) {
            addCriterion("menu_craft in", values, "menuCraft");
            return (Criteria) this;
        }

        public Criteria andMenuCraftNotIn(List<String> values) {
            addCriterion("menu_craft not in", values, "menuCraft");
            return (Criteria) this;
        }

        public Criteria andMenuCraftBetween(String value1, String value2) {
            addCriterion("menu_craft between", value1, value2, "menuCraft");
            return (Criteria) this;
        }

        public Criteria andMenuCraftNotBetween(String value1, String value2) {
            addCriterion("menu_craft not between", value1, value2, "menuCraft");
            return (Criteria) this;
        }

        public Criteria andMenuTasteIsNull() {
            addCriterion("menu_taste is null");
            return (Criteria) this;
        }

        public Criteria andMenuTasteIsNotNull() {
            addCriterion("menu_taste is not null");
            return (Criteria) this;
        }

        public Criteria andMenuTasteEqualTo(String value) {
            addCriterion("menu_taste =", value, "menuTaste");
            return (Criteria) this;
        }

        public Criteria andMenuTasteNotEqualTo(String value) {
            addCriterion("menu_taste <>", value, "menuTaste");
            return (Criteria) this;
        }

        public Criteria andMenuTasteGreaterThan(String value) {
            addCriterion("menu_taste >", value, "menuTaste");
            return (Criteria) this;
        }

        public Criteria andMenuTasteGreaterThanOrEqualTo(String value) {
            addCriterion("menu_taste >=", value, "menuTaste");
            return (Criteria) this;
        }

        public Criteria andMenuTasteLessThan(String value) {
            addCriterion("menu_taste <", value, "menuTaste");
            return (Criteria) this;
        }

        public Criteria andMenuTasteLessThanOrEqualTo(String value) {
            addCriterion("menu_taste <=", value, "menuTaste");
            return (Criteria) this;
        }

        public Criteria andMenuTasteLike(String value) {
            addCriterion("menu_taste like", value, "menuTaste");
            return (Criteria) this;
        }

        public Criteria andMenuTasteNotLike(String value) {
            addCriterion("menu_taste not like", value, "menuTaste");
            return (Criteria) this;
        }

        public Criteria andMenuTasteIn(List<String> values) {
            addCriterion("menu_taste in", values, "menuTaste");
            return (Criteria) this;
        }

        public Criteria andMenuTasteNotIn(List<String> values) {
            addCriterion("menu_taste not in", values, "menuTaste");
            return (Criteria) this;
        }

        public Criteria andMenuTasteBetween(String value1, String value2) {
            addCriterion("menu_taste between", value1, value2, "menuTaste");
            return (Criteria) this;
        }

        public Criteria andMenuTasteNotBetween(String value1, String value2) {
            addCriterion("menu_taste not between", value1, value2, "menuTaste");
            return (Criteria) this;
        }

        public Criteria andMenuTimeIsNull() {
            addCriterion("menu_time is null");
            return (Criteria) this;
        }

        public Criteria andMenuTimeIsNotNull() {
            addCriterion("menu_time is not null");
            return (Criteria) this;
        }

        public Criteria andMenuTimeEqualTo(String value) {
            addCriterion("menu_time =", value, "menuTime");
            return (Criteria) this;
        }

        public Criteria andMenuTimeNotEqualTo(String value) {
            addCriterion("menu_time <>", value, "menuTime");
            return (Criteria) this;
        }

        public Criteria andMenuTimeGreaterThan(String value) {
            addCriterion("menu_time >", value, "menuTime");
            return (Criteria) this;
        }

        public Criteria andMenuTimeGreaterThanOrEqualTo(String value) {
            addCriterion("menu_time >=", value, "menuTime");
            return (Criteria) this;
        }

        public Criteria andMenuTimeLessThan(String value) {
            addCriterion("menu_time <", value, "menuTime");
            return (Criteria) this;
        }

        public Criteria andMenuTimeLessThanOrEqualTo(String value) {
            addCriterion("menu_time <=", value, "menuTime");
            return (Criteria) this;
        }

        public Criteria andMenuTimeLike(String value) {
            addCriterion("menu_time like", value, "menuTime");
            return (Criteria) this;
        }

        public Criteria andMenuTimeNotLike(String value) {
            addCriterion("menu_time not like", value, "menuTime");
            return (Criteria) this;
        }

        public Criteria andMenuTimeIn(List<String> values) {
            addCriterion("menu_time in", values, "menuTime");
            return (Criteria) this;
        }

        public Criteria andMenuTimeNotIn(List<String> values) {
            addCriterion("menu_time not in", values, "menuTime");
            return (Criteria) this;
        }

        public Criteria andMenuTimeBetween(String value1, String value2) {
            addCriterion("menu_time between", value1, value2, "menuTime");
            return (Criteria) this;
        }

        public Criteria andMenuTimeNotBetween(String value1, String value2) {
            addCriterion("menu_time not between", value1, value2, "menuTime");
            return (Criteria) this;
        }

        public Criteria andMainIngredientIsNull() {
            addCriterion("main_ingredient is null");
            return (Criteria) this;
        }

        public Criteria andMainIngredientIsNotNull() {
            addCriterion("main_ingredient is not null");
            return (Criteria) this;
        }

        public Criteria andMainIngredientEqualTo(String value) {
            addCriterion("main_ingredient =", value, "mainIngredient");
            return (Criteria) this;
        }

        public Criteria andMainIngredientNotEqualTo(String value) {
            addCriterion("main_ingredient <>", value, "mainIngredient");
            return (Criteria) this;
        }

        public Criteria andMainIngredientGreaterThan(String value) {
            addCriterion("main_ingredient >", value, "mainIngredient");
            return (Criteria) this;
        }

        public Criteria andMainIngredientGreaterThanOrEqualTo(String value) {
            addCriterion("main_ingredient >=", value, "mainIngredient");
            return (Criteria) this;
        }

        public Criteria andMainIngredientLessThan(String value) {
            addCriterion("main_ingredient <", value, "mainIngredient");
            return (Criteria) this;
        }

        public Criteria andMainIngredientLessThanOrEqualTo(String value) {
            addCriterion("main_ingredient <=", value, "mainIngredient");
            return (Criteria) this;
        }

        public Criteria andMainIngredientLike(String value) {
            addCriterion("main_ingredient like", value, "mainIngredient");
            return (Criteria) this;
        }

        public Criteria andMainIngredientNotLike(String value) {
            addCriterion("main_ingredient not like", value, "mainIngredient");
            return (Criteria) this;
        }

        public Criteria andMainIngredientIn(List<String> values) {
            addCriterion("main_ingredient in", values, "mainIngredient");
            return (Criteria) this;
        }

        public Criteria andMainIngredientNotIn(List<String> values) {
            addCriterion("main_ingredient not in", values, "mainIngredient");
            return (Criteria) this;
        }

        public Criteria andMainIngredientBetween(String value1, String value2) {
            addCriterion("main_ingredient between", value1, value2, "mainIngredient");
            return (Criteria) this;
        }

        public Criteria andMainIngredientNotBetween(String value1, String value2) {
            addCriterion("main_ingredient not between", value1, value2, "mainIngredient");
            return (Criteria) this;
        }

        public Criteria andMinorIngredientIsNull() {
            addCriterion("minor_ingredient is null");
            return (Criteria) this;
        }

        public Criteria andMinorIngredientIsNotNull() {
            addCriterion("minor_ingredient is not null");
            return (Criteria) this;
        }

        public Criteria andMinorIngredientEqualTo(String value) {
            addCriterion("minor_ingredient =", value, "minorIngredient");
            return (Criteria) this;
        }

        public Criteria andMinorIngredientNotEqualTo(String value) {
            addCriterion("minor_ingredient <>", value, "minorIngredient");
            return (Criteria) this;
        }

        public Criteria andMinorIngredientGreaterThan(String value) {
            addCriterion("minor_ingredient >", value, "minorIngredient");
            return (Criteria) this;
        }

        public Criteria andMinorIngredientGreaterThanOrEqualTo(String value) {
            addCriterion("minor_ingredient >=", value, "minorIngredient");
            return (Criteria) this;
        }

        public Criteria andMinorIngredientLessThan(String value) {
            addCriterion("minor_ingredient <", value, "minorIngredient");
            return (Criteria) this;
        }

        public Criteria andMinorIngredientLessThanOrEqualTo(String value) {
            addCriterion("minor_ingredient <=", value, "minorIngredient");
            return (Criteria) this;
        }

        public Criteria andMinorIngredientLike(String value) {
            addCriterion("minor_ingredient like", value, "minorIngredient");
            return (Criteria) this;
        }

        public Criteria andMinorIngredientNotLike(String value) {
            addCriterion("minor_ingredient not like", value, "minorIngredient");
            return (Criteria) this;
        }

        public Criteria andMinorIngredientIn(List<String> values) {
            addCriterion("minor_ingredient in", values, "minorIngredient");
            return (Criteria) this;
        }

        public Criteria andMinorIngredientNotIn(List<String> values) {
            addCriterion("minor_ingredient not in", values, "minorIngredient");
            return (Criteria) this;
        }

        public Criteria andMinorIngredientBetween(String value1, String value2) {
            addCriterion("minor_ingredient between", value1, value2, "minorIngredient");
            return (Criteria) this;
        }

        public Criteria andMinorIngredientNotBetween(String value1, String value2) {
            addCriterion("minor_ingredient not between", value1, value2, "minorIngredient");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}