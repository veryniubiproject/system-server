package top.pengdong.dachuang.main.fegin;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import top.pengdong.common.utils.R;

/**
 * @description:
 * @projectName: dachuang
 * @see: top.pengdong.dachuang.service
 * @author: pc
 * @createTime: 2022/3/14 18:37
 * @version: 1.0
 */
@FeignClient("dachuang-menu8000")
public interface CommentService {

	@RequestMapping("/menu/comment/list")
	public R getCommentList();

}
