package top.pengdong.dachuang.main.mbg.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Comment implements Serializable {
    /**
     * 评论ID
     *
     * @mbggenerated
     */
    private Long commentId;

    /**
     * 评论时间
     *
     * @mbggenerated
     */
    private Date commentTime;

    /**
     * 评论分数
     *
     * @mbggenerated
     */
    private BigDecimal commentScore;

    /**
     * 评论内容
     *
     * @mbggenerated
     */
    private String commentDetail;

    private static final long serialVersionUID = 1L;

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public Date getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(Date commentTime) {
        this.commentTime = commentTime;
    }

    public BigDecimal getCommentScore() {
        return commentScore;
    }

    public void setCommentScore(BigDecimal commentScore) {
        this.commentScore = commentScore;
    }

    public String getCommentDetail() {
        return commentDetail;
    }

    public void setCommentDetail(String commentDetail) {
        this.commentDetail = commentDetail;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", commentId=").append(commentId);
        sb.append(", commentTime=").append(commentTime);
        sb.append(", commentScore=").append(commentScore);
        sb.append(", commentDetail=").append(commentDetail);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}