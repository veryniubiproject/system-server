package top.pengdong.dachuang.main.mbg.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import top.pengdong.dachuang.main.mbg.model.StepMenu;
import top.pengdong.dachuang.main.mbg.model.StepMenuExample;

public interface StepMenuMapper {
    int countByExample(StepMenuExample example);

    int deleteByExample(StepMenuExample example);

    int deleteByPrimaryKey(Long id);

    int insert(StepMenu record);

    int insertSelective(StepMenu record);

    List<StepMenu> selectByExample(StepMenuExample example);

    StepMenu selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") StepMenu record, @Param("example") StepMenuExample example);

    int updateByExample(@Param("record") StepMenu record, @Param("example") StepMenuExample example);

    int updateByPrimaryKeySelective(StepMenu record);

    int updateByPrimaryKey(StepMenu record);
}