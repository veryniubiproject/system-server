package top.pengdong.dachuang.main.mbg.model;

import java.io.Serializable;

public class DishComment implements Serializable {
    /**
     * 表ID
     *
     * @mbggenerated
     */
    private Long id;

    /**
     * 菜品ID
     *
     * @mbggenerated
     */
    private Long dishId;

    /**
     * 评论ID
     *
     * @mbggenerated
     */
    private Long commentId;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDishId() {
        return dishId;
    }

    public void setDishId(Long dishId) {
        this.dishId = dishId;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", dishId=").append(dishId);
        sb.append(", commentId=").append(commentId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}