package top.pengdong.dachuang.main.mbg.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Dish implements Serializable {
    /**
     * 菜品ID
     *
     * @mbggenerated
     */
    private Long dishId;

    /**
     * 菜品名字
     *
     * @mbggenerated
     */
    private String dishName;

    /**
     * 菜品价格
     *
     * @mbggenerated
     */
    private BigDecimal dishPrice;

    /**
     * 菜品类型（八大菜系）
     *
     * @mbggenerated
     */
    private String dishCate;

    /**
     * 菜品适宜人群
     *
     * @mbggenerated
     */
    private String suitableUser;

    /**
     * 菜品标签（特色说明）
     *
     * @mbggenerated
     */
    private String dishFeature;

    /**
     * 菜品材料
     *
     * @mbggenerated
     */
    private String dishMaterial;

    private static final long serialVersionUID = 1L;

    public Long getDishId() {
        return dishId;
    }

    public void setDishId(Long dishId) {
        this.dishId = dishId;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public BigDecimal getDishPrice() {
        return dishPrice;
    }

    public void setDishPrice(BigDecimal dishPrice) {
        this.dishPrice = dishPrice;
    }

    public String getDishCate() {
        return dishCate;
    }

    public void setDishCate(String dishCate) {
        this.dishCate = dishCate;
    }

    public String getSuitableUser() {
        return suitableUser;
    }

    public void setSuitableUser(String suitableUser) {
        this.suitableUser = suitableUser;
    }

    public String getDishFeature() {
        return dishFeature;
    }

    public void setDishFeature(String dishFeature) {
        this.dishFeature = dishFeature;
    }

    public String getDishMaterial() {
        return dishMaterial;
    }

    public void setDishMaterial(String dishMaterial) {
        this.dishMaterial = dishMaterial;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", dishId=").append(dishId);
        sb.append(", dishName=").append(dishName);
        sb.append(", dishPrice=").append(dishPrice);
        sb.append(", dishCate=").append(dishCate);
        sb.append(", suitableUser=").append(suitableUser);
        sb.append(", dishFeature=").append(dishFeature);
        sb.append(", dishMaterial=").append(dishMaterial);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}