package top.pengdong.dachuang.main.mbg.model;

import java.io.Serializable;

public class StepMenu implements Serializable {
    /**
     * 表ID
     *
     * @mbggenerated
     */
    private Long id;

    /**
     * 步骤ID
     *
     * @mbggenerated
     */
    private Long stepId;

    /**
     * 菜谱ID
     *
     * @mbggenerated
     */
    private Long menuId;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStepId() {
        return stepId;
    }

    public void setStepId(Long stepId) {
        this.stepId = stepId;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", stepId=").append(stepId);
        sb.append(", menuId=").append(menuId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}