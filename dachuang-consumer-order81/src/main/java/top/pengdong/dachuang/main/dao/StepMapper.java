package top.pengdong.dachuang.main.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import top.pengdong.dachuang.main.mbg.model.Step;
import top.pengdong.dachuang.main.mbg.model.StepExample;

public interface StepMapper {
    int countByExample(StepExample example);

    int deleteByExample(StepExample example);

    int deleteByPrimaryKey(Long stepId);

    int insert(Step record);

    int insertSelective(Step record);

    List<Step> selectByExample(StepExample example);

    Step selectByPrimaryKey(Long stepId);

    int updateByExampleSelective(@Param("record") Step record, @Param("example") StepExample example);

    int updateByExample(@Param("record") Step record, @Param("example") StepExample example);

    int updateByPrimaryKeySelective(Step record);

    int updateByPrimaryKey(Step record);
}