package top.pengdong.dachuang.main.handle;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;
import top.pengdong.common.utils.ResultCode;
import top.pengdong.common.utils.ResultResponse;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @description: 当访问接口没有权限时候
 * @projectName: RealHole
 * @see: com.pengshi.realhole.handle
 * @author: pc
 * @createTime: 2022/2/11 22:03
 * @version: 1.0
 */
@Component
public class RestfulAccessDeniedHandle implements AccessDeniedHandler {
	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		out.write(new ObjectMapper().writeValueAsString(ResultResponse.fail(ResultCode.SIGN_ERROR)));
		out.flush();
		out.close();
	}
}
