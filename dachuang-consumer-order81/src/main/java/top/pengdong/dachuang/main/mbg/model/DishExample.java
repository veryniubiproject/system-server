package top.pengdong.dachuang.main.mbg.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class DishExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public DishExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andDishIdIsNull() {
            addCriterion("dish_id is null");
            return (Criteria) this;
        }

        public Criteria andDishIdIsNotNull() {
            addCriterion("dish_id is not null");
            return (Criteria) this;
        }

        public Criteria andDishIdEqualTo(Long value) {
            addCriterion("dish_id =", value, "dishId");
            return (Criteria) this;
        }

        public Criteria andDishIdNotEqualTo(Long value) {
            addCriterion("dish_id <>", value, "dishId");
            return (Criteria) this;
        }

        public Criteria andDishIdGreaterThan(Long value) {
            addCriterion("dish_id >", value, "dishId");
            return (Criteria) this;
        }

        public Criteria andDishIdGreaterThanOrEqualTo(Long value) {
            addCriterion("dish_id >=", value, "dishId");
            return (Criteria) this;
        }

        public Criteria andDishIdLessThan(Long value) {
            addCriterion("dish_id <", value, "dishId");
            return (Criteria) this;
        }

        public Criteria andDishIdLessThanOrEqualTo(Long value) {
            addCriterion("dish_id <=", value, "dishId");
            return (Criteria) this;
        }

        public Criteria andDishIdIn(List<Long> values) {
            addCriterion("dish_id in", values, "dishId");
            return (Criteria) this;
        }

        public Criteria andDishIdNotIn(List<Long> values) {
            addCriterion("dish_id not in", values, "dishId");
            return (Criteria) this;
        }

        public Criteria andDishIdBetween(Long value1, Long value2) {
            addCriterion("dish_id between", value1, value2, "dishId");
            return (Criteria) this;
        }

        public Criteria andDishIdNotBetween(Long value1, Long value2) {
            addCriterion("dish_id not between", value1, value2, "dishId");
            return (Criteria) this;
        }

        public Criteria andDishNameIsNull() {
            addCriterion("dish_name is null");
            return (Criteria) this;
        }

        public Criteria andDishNameIsNotNull() {
            addCriterion("dish_name is not null");
            return (Criteria) this;
        }

        public Criteria andDishNameEqualTo(String value) {
            addCriterion("dish_name =", value, "dishName");
            return (Criteria) this;
        }

        public Criteria andDishNameNotEqualTo(String value) {
            addCriterion("dish_name <>", value, "dishName");
            return (Criteria) this;
        }

        public Criteria andDishNameGreaterThan(String value) {
            addCriterion("dish_name >", value, "dishName");
            return (Criteria) this;
        }

        public Criteria andDishNameGreaterThanOrEqualTo(String value) {
            addCriterion("dish_name >=", value, "dishName");
            return (Criteria) this;
        }

        public Criteria andDishNameLessThan(String value) {
            addCriterion("dish_name <", value, "dishName");
            return (Criteria) this;
        }

        public Criteria andDishNameLessThanOrEqualTo(String value) {
            addCriterion("dish_name <=", value, "dishName");
            return (Criteria) this;
        }

        public Criteria andDishNameLike(String value) {
            addCriterion("dish_name like", value, "dishName");
            return (Criteria) this;
        }

        public Criteria andDishNameNotLike(String value) {
            addCriterion("dish_name not like", value, "dishName");
            return (Criteria) this;
        }

        public Criteria andDishNameIn(List<String> values) {
            addCriterion("dish_name in", values, "dishName");
            return (Criteria) this;
        }

        public Criteria andDishNameNotIn(List<String> values) {
            addCriterion("dish_name not in", values, "dishName");
            return (Criteria) this;
        }

        public Criteria andDishNameBetween(String value1, String value2) {
            addCriterion("dish_name between", value1, value2, "dishName");
            return (Criteria) this;
        }

        public Criteria andDishNameNotBetween(String value1, String value2) {
            addCriterion("dish_name not between", value1, value2, "dishName");
            return (Criteria) this;
        }

        public Criteria andDishPriceIsNull() {
            addCriterion("dish_price is null");
            return (Criteria) this;
        }

        public Criteria andDishPriceIsNotNull() {
            addCriterion("dish_price is not null");
            return (Criteria) this;
        }

        public Criteria andDishPriceEqualTo(BigDecimal value) {
            addCriterion("dish_price =", value, "dishPrice");
            return (Criteria) this;
        }

        public Criteria andDishPriceNotEqualTo(BigDecimal value) {
            addCriterion("dish_price <>", value, "dishPrice");
            return (Criteria) this;
        }

        public Criteria andDishPriceGreaterThan(BigDecimal value) {
            addCriterion("dish_price >", value, "dishPrice");
            return (Criteria) this;
        }

        public Criteria andDishPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("dish_price >=", value, "dishPrice");
            return (Criteria) this;
        }

        public Criteria andDishPriceLessThan(BigDecimal value) {
            addCriterion("dish_price <", value, "dishPrice");
            return (Criteria) this;
        }

        public Criteria andDishPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("dish_price <=", value, "dishPrice");
            return (Criteria) this;
        }

        public Criteria andDishPriceIn(List<BigDecimal> values) {
            addCriterion("dish_price in", values, "dishPrice");
            return (Criteria) this;
        }

        public Criteria andDishPriceNotIn(List<BigDecimal> values) {
            addCriterion("dish_price not in", values, "dishPrice");
            return (Criteria) this;
        }

        public Criteria andDishPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("dish_price between", value1, value2, "dishPrice");
            return (Criteria) this;
        }

        public Criteria andDishPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("dish_price not between", value1, value2, "dishPrice");
            return (Criteria) this;
        }

        public Criteria andDishCateIsNull() {
            addCriterion("dish_cate is null");
            return (Criteria) this;
        }

        public Criteria andDishCateIsNotNull() {
            addCriterion("dish_cate is not null");
            return (Criteria) this;
        }

        public Criteria andDishCateEqualTo(String value) {
            addCriterion("dish_cate =", value, "dishCate");
            return (Criteria) this;
        }

        public Criteria andDishCateNotEqualTo(String value) {
            addCriterion("dish_cate <>", value, "dishCate");
            return (Criteria) this;
        }

        public Criteria andDishCateGreaterThan(String value) {
            addCriterion("dish_cate >", value, "dishCate");
            return (Criteria) this;
        }

        public Criteria andDishCateGreaterThanOrEqualTo(String value) {
            addCriterion("dish_cate >=", value, "dishCate");
            return (Criteria) this;
        }

        public Criteria andDishCateLessThan(String value) {
            addCriterion("dish_cate <", value, "dishCate");
            return (Criteria) this;
        }

        public Criteria andDishCateLessThanOrEqualTo(String value) {
            addCriterion("dish_cate <=", value, "dishCate");
            return (Criteria) this;
        }

        public Criteria andDishCateLike(String value) {
            addCriterion("dish_cate like", value, "dishCate");
            return (Criteria) this;
        }

        public Criteria andDishCateNotLike(String value) {
            addCriterion("dish_cate not like", value, "dishCate");
            return (Criteria) this;
        }

        public Criteria andDishCateIn(List<String> values) {
            addCriterion("dish_cate in", values, "dishCate");
            return (Criteria) this;
        }

        public Criteria andDishCateNotIn(List<String> values) {
            addCriterion("dish_cate not in", values, "dishCate");
            return (Criteria) this;
        }

        public Criteria andDishCateBetween(String value1, String value2) {
            addCriterion("dish_cate between", value1, value2, "dishCate");
            return (Criteria) this;
        }

        public Criteria andDishCateNotBetween(String value1, String value2) {
            addCriterion("dish_cate not between", value1, value2, "dishCate");
            return (Criteria) this;
        }

        public Criteria andSuitableUserIsNull() {
            addCriterion("suitable_user is null");
            return (Criteria) this;
        }

        public Criteria andSuitableUserIsNotNull() {
            addCriterion("suitable_user is not null");
            return (Criteria) this;
        }

        public Criteria andSuitableUserEqualTo(String value) {
            addCriterion("suitable_user =", value, "suitableUser");
            return (Criteria) this;
        }

        public Criteria andSuitableUserNotEqualTo(String value) {
            addCriterion("suitable_user <>", value, "suitableUser");
            return (Criteria) this;
        }

        public Criteria andSuitableUserGreaterThan(String value) {
            addCriterion("suitable_user >", value, "suitableUser");
            return (Criteria) this;
        }

        public Criteria andSuitableUserGreaterThanOrEqualTo(String value) {
            addCriterion("suitable_user >=", value, "suitableUser");
            return (Criteria) this;
        }

        public Criteria andSuitableUserLessThan(String value) {
            addCriterion("suitable_user <", value, "suitableUser");
            return (Criteria) this;
        }

        public Criteria andSuitableUserLessThanOrEqualTo(String value) {
            addCriterion("suitable_user <=", value, "suitableUser");
            return (Criteria) this;
        }

        public Criteria andSuitableUserLike(String value) {
            addCriterion("suitable_user like", value, "suitableUser");
            return (Criteria) this;
        }

        public Criteria andSuitableUserNotLike(String value) {
            addCriterion("suitable_user not like", value, "suitableUser");
            return (Criteria) this;
        }

        public Criteria andSuitableUserIn(List<String> values) {
            addCriterion("suitable_user in", values, "suitableUser");
            return (Criteria) this;
        }

        public Criteria andSuitableUserNotIn(List<String> values) {
            addCriterion("suitable_user not in", values, "suitableUser");
            return (Criteria) this;
        }

        public Criteria andSuitableUserBetween(String value1, String value2) {
            addCriterion("suitable_user between", value1, value2, "suitableUser");
            return (Criteria) this;
        }

        public Criteria andSuitableUserNotBetween(String value1, String value2) {
            addCriterion("suitable_user not between", value1, value2, "suitableUser");
            return (Criteria) this;
        }

        public Criteria andDishFeatureIsNull() {
            addCriterion("dish_feature is null");
            return (Criteria) this;
        }

        public Criteria andDishFeatureIsNotNull() {
            addCriterion("dish_feature is not null");
            return (Criteria) this;
        }

        public Criteria andDishFeatureEqualTo(String value) {
            addCriterion("dish_feature =", value, "dishFeature");
            return (Criteria) this;
        }

        public Criteria andDishFeatureNotEqualTo(String value) {
            addCriterion("dish_feature <>", value, "dishFeature");
            return (Criteria) this;
        }

        public Criteria andDishFeatureGreaterThan(String value) {
            addCriterion("dish_feature >", value, "dishFeature");
            return (Criteria) this;
        }

        public Criteria andDishFeatureGreaterThanOrEqualTo(String value) {
            addCriterion("dish_feature >=", value, "dishFeature");
            return (Criteria) this;
        }

        public Criteria andDishFeatureLessThan(String value) {
            addCriterion("dish_feature <", value, "dishFeature");
            return (Criteria) this;
        }

        public Criteria andDishFeatureLessThanOrEqualTo(String value) {
            addCriterion("dish_feature <=", value, "dishFeature");
            return (Criteria) this;
        }

        public Criteria andDishFeatureLike(String value) {
            addCriterion("dish_feature like", value, "dishFeature");
            return (Criteria) this;
        }

        public Criteria andDishFeatureNotLike(String value) {
            addCriterion("dish_feature not like", value, "dishFeature");
            return (Criteria) this;
        }

        public Criteria andDishFeatureIn(List<String> values) {
            addCriterion("dish_feature in", values, "dishFeature");
            return (Criteria) this;
        }

        public Criteria andDishFeatureNotIn(List<String> values) {
            addCriterion("dish_feature not in", values, "dishFeature");
            return (Criteria) this;
        }

        public Criteria andDishFeatureBetween(String value1, String value2) {
            addCriterion("dish_feature between", value1, value2, "dishFeature");
            return (Criteria) this;
        }

        public Criteria andDishFeatureNotBetween(String value1, String value2) {
            addCriterion("dish_feature not between", value1, value2, "dishFeature");
            return (Criteria) this;
        }

        public Criteria andDishMaterialIsNull() {
            addCriterion("dish_material is null");
            return (Criteria) this;
        }

        public Criteria andDishMaterialIsNotNull() {
            addCriterion("dish_material is not null");
            return (Criteria) this;
        }

        public Criteria andDishMaterialEqualTo(String value) {
            addCriterion("dish_material =", value, "dishMaterial");
            return (Criteria) this;
        }

        public Criteria andDishMaterialNotEqualTo(String value) {
            addCriterion("dish_material <>", value, "dishMaterial");
            return (Criteria) this;
        }

        public Criteria andDishMaterialGreaterThan(String value) {
            addCriterion("dish_material >", value, "dishMaterial");
            return (Criteria) this;
        }

        public Criteria andDishMaterialGreaterThanOrEqualTo(String value) {
            addCriterion("dish_material >=", value, "dishMaterial");
            return (Criteria) this;
        }

        public Criteria andDishMaterialLessThan(String value) {
            addCriterion("dish_material <", value, "dishMaterial");
            return (Criteria) this;
        }

        public Criteria andDishMaterialLessThanOrEqualTo(String value) {
            addCriterion("dish_material <=", value, "dishMaterial");
            return (Criteria) this;
        }

        public Criteria andDishMaterialLike(String value) {
            addCriterion("dish_material like", value, "dishMaterial");
            return (Criteria) this;
        }

        public Criteria andDishMaterialNotLike(String value) {
            addCriterion("dish_material not like", value, "dishMaterial");
            return (Criteria) this;
        }

        public Criteria andDishMaterialIn(List<String> values) {
            addCriterion("dish_material in", values, "dishMaterial");
            return (Criteria) this;
        }

        public Criteria andDishMaterialNotIn(List<String> values) {
            addCriterion("dish_material not in", values, "dishMaterial");
            return (Criteria) this;
        }

        public Criteria andDishMaterialBetween(String value1, String value2) {
            addCriterion("dish_material between", value1, value2, "dishMaterial");
            return (Criteria) this;
        }

        public Criteria andDishMaterialNotBetween(String value1, String value2) {
            addCriterion("dish_material not between", value1, value2, "dishMaterial");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}