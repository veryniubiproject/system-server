package top.pengdong.dachuang.main.mbg.model;

import java.io.Serializable;

public class MenuDish implements Serializable {
    /**
     * 表ID
     *
     * @mbggenerated
     */
    private Long id;

    /**
     * 菜谱ID
     *
     * @mbggenerated
     */
    private Long menuId;

    /**
     * 菜品ID
     *
     * @mbggenerated
     */
    private Long dishId;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public Long getDishId() {
        return dishId;
    }

    public void setDishId(Long dishId) {
        this.dishId = dishId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", menuId=").append(menuId);
        sb.append(", dishId=").append(dishId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}