package top.pengdong.dachuang.main.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import top.pengdong.dachuang.main.mbg.model.Dish;
import top.pengdong.dachuang.main.mbg.model.DishExample;

public interface DishMapper {
    int countByExample(DishExample example);

    int deleteByExample(DishExample example);

    int deleteByPrimaryKey(Long dishId);

    int insert(Dish record);

    int insertSelective(Dish record);

    List<Dish> selectByExample(DishExample example);

    Dish selectByPrimaryKey(Long dishId);

    int updateByExampleSelective(@Param("record") Dish record, @Param("example") DishExample example);

    int updateByExample(@Param("record") Dish record, @Param("example") DishExample example);

    int updateByPrimaryKeySelective(Dish record);

    int updateByPrimaryKey(Dish record);
}