package top.pengdong.dachuang.main.service;

import top.pengdong.common.utils.ResultResponse;
import top.pengdong.common.pojo.User;

/**
 * @description:
 * @projectName: dachuang
 * @see: top.pengdong.dachuang.service
 * @author: pc
 * @createTime: 2022/3/11 10:16
 * @version: 1.0
 */
public interface UserService {
	/**
	 * 用户登出操作
	 * @param username
	 * @return
	 */
	ResultResponse logout(String username);

	/**
	 * 用户登陆操作
	 * @param username
	 * @param password
	 * @param code
	 * @return
	 */
	ResultResponse login(String username, String password, String code);

	/**
	 * 用户查询操作
	 * @param username
	 * @return
	 */
	User getUserByUsername(String username);

	/**
	 * 获取用户信息
	 * @return
	 */
	ResultResponse getUserInfo(String headerValue);
}
